import pytest
from main import formStr
from fastapi.testclient import TestClient
from main import app


client = TestClient(app)


def test_string0():
    response = client.get("/foramtStr/?name=Konstantin&age=18&group=BVT2102")
    assert response.status_code == 200
    print(response.json())
    assert response.json() == {"msg": 'User profile: name = Konstantin, age = 18, group = BVT2102'}

def test_string1():
    response = client.get("/foramtStr/?name=kst")
    assert response.status_code == 200
    print(response.json())
    assert response.json() == {"msg": "User profile: name = kst, age = 10, group = not entered"}


def test_string2():
    response = client.get("/foramtStr/?name=test&age=100&group=123")
    assert response.status_code == 200
    print(response.json())
    assert response.json() == {"msg": "User profile: name = test, age = 100, group = 123"}


def test_string3():
    response = client.get("/foramtStr/")
    assert response.status_code == 200
    print(response.json())
    assert response.json() == {"msg": "User profile: name = testName, age = 10, group = not entered"}


